package com.urancompany.challenge.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.urancompany.challenge.R;


/**
 * Created by ovi on 4/18/16.
 */
abstract class BaseActivity extends AppCompatActivity {

    protected final void startFragment(Fragment fragment, boolean addToBackStack){
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        if(addToBackStack)
            fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());

        fragmentTransaction.commit();
    }

}
