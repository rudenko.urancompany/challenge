package com.urancompany.challenge.models.api.listeners;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;

/**
 * Created by ovitali on 13.10.2015.
 */
public abstract class SimplePendingRequestListener<RESULT> implements PendingRequestListener<RESULT> {
    @Override
    public void onRequestNotFound() {

    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(RESULT result) {

    }
}
