package com.urancompany.challenge.models.api.sets;

import com.urancompany.challenge.models.entities.SampleModel;

import retrofit.http.POST;

/**
 * Created by ovi on 4/18/16.
 */
public interface ApiSet {

    @POST("/")
    SampleModel[] getModels();
}
