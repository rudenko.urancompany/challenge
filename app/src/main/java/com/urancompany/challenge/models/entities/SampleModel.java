package com.urancompany.challenge.models.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ovi on 4/18/16.
 */
public class SampleModel extends BaseModel{

    @DatabaseField(columnName = "title")
    private String mTitle;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }
}
