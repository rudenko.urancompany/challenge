package com.urancompany.challenge.models.db.loaders;

import android.app.LoaderManager;
import android.content.Loader;

/**
 * Created by ovi on 4/18/16.
 */
public abstract class SimpleLoaderCallbacks<T> implements LoaderManager.LoaderCallbacks<T> {

    @Override
    public void onLoaderReset(Loader<T> loader) {

    }
}
