package com.urancompany.challenge.models.api;

import android.util.Log;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import roboguice.util.temp.Ln;

public final class AppService extends RetrofitGsonSpiceService {

    @Override
    public int getThreadCount() {
        return 2;
    }


    @Override
    protected String getServerUrl() {
        return "http://";
    }

    protected RestAdapter.Builder createRestAdapterBuilder() {
        Ln.getConfig().setLoggingLevel(Log.ERROR);

        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setClient(new OkClient(new OkHttpClient()))
                .setEndpoint(getServerUrl())
                .setConverter(getConverter());
    }


}
