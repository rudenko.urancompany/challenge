package com.urancompany.challenge.models.db.loaders;

import android.content.Context;

import com.urancompany.challenge.models.db.DatabaseManager;
import com.urancompany.challenge.models.entities.SampleModel;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by ovi on 4/18/16.
 */
public class SampleListLoader extends BaseLoader<List<SampleModel>>{

    public SampleListLoader(Context context) {
        super(context);
    }

    @Override
    public List<SampleModel> loadInBackground() {
        try {
            return DatabaseManager.getsInstance().getHelper().getSampleDao().queryForAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
