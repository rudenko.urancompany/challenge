package com.urancompany.challenge.models.api.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.urancompany.challenge.models.api.sets.ApiSet;
import com.urancompany.challenge.models.db.DatabaseManager;
import com.urancompany.challenge.models.entities.SampleModel;

/**
 * Created by ovi on 4/18/16.
 */
public class GetSampleRequest extends RetrofitSpiceRequest<SampleModel[], ApiSet> {

    public GetSampleRequest() {
        super(SampleModel[].class, ApiSet.class);
    }

    @Override
    public SampleModel[] loadDataFromNetwork() throws Exception {
        int limit = 1000;
        SampleModel[] models = new SampleModel[limit];
        for (int i = 0; i < limit; i++) {
            models[i] = new SampleModel();
            models[i].setId(i + 1);
            models[i].setTitle("abcdefgh  " + (i + 1));
            DatabaseManager.getsInstance().getHelper().getSampleDao().createOrUpdate(models[i]);
        }

        return models;
    }
}
