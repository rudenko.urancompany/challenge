package com.urancompany.challenge.models.db.loaders;

import android.content.Context;
import android.content.AsyncTaskLoader;

/**
 * Created by ovitali on 09.07.2015.
 * Project is Motivator
 */
public abstract class BaseLoader<T> extends AsyncTaskLoader<T> {

    protected String[] mWheres;

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }


    public BaseLoader(Context context) {
        super(context);
    }

    protected void addCondition(String condition){
        if (mWheres == null){
            mWheres = new String[1];
            mWheres[0] = condition;
        }else{
            String[] wheres = new String[mWheres.length+1];
            int i= 0;
            for (; i <  mWheres.length; i++){
                wheres[i] = mWheres[i];
            }
            wheres[i] = condition;

            mWheres = wheres;
        }

    }
}
