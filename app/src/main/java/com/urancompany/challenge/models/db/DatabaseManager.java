package com.urancompany.challenge.models.db;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ovitali on 27.03.2015.
 */
public class DatabaseManager {

    private static ExecutorService sExecutorService;

    private static volatile DatabaseManager sInstance;
    private DatabaseHelper mHelper;

    private DatabaseManager() {
    }

    public static DatabaseManager getsInstance() {
        if (sInstance == null) {
            synchronized (DatabaseManager.class) {
                if (sInstance == null) {
                    sInstance = new DatabaseManager();
                }
            }
        }
        return sInstance;

    }

    public void init(Context context) {
        if (mHelper == null)
            mHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

    public void release() {
        if (mHelper != null)
            OpenHelperManager.releaseHelper();
    }

    public DatabaseHelper getHelper() {
        return mHelper;
    }

    private static ExecutorService getExecutorService() {
        if (sExecutorService == null) {
            sExecutorService = Executors.newFixedThreadPool(1);
        }
        return sExecutorService;
    }

    public static void submit(Runnable task) {
        getExecutorService().submit(task);
    }

    public static void cancelExecutor() {
        if (sExecutorService == null)
            return;

        sExecutorService.shutdownNow();
        sExecutorService = null;
    }

}