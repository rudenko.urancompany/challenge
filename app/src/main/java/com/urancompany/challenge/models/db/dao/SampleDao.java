package com.urancompany.challenge.models.db.dao;

import com.j256.ormlite.support.ConnectionSource;
import com.urancompany.challenge.models.entities.SampleModel;

import java.sql.SQLException;

/**
 * Created by ovi on 4/18/16.
 */
public class SampleDao extends BaseDao<SampleModel> {
    public SampleDao(ConnectionSource source) throws SQLException {
        super(source, SampleModel.class);
    }
}
