package com.urancompany.challenge.models.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ovi on 4/18/16.
 */
public abstract class BaseModel {

    @DatabaseField(generatedId = true, columnName = "_id")
    private int mId;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }
}
