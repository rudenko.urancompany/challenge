package com.urancompany.challenge.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.urancompany.challenge.R;
import com.urancompany.challenge.databinding.ItemSampleBinding;
import com.urancompany.challenge.models.entities.SampleModel;

import java.util.List;


/**
 * Created by ovi on 4/18/16.
 */
public class SampleRecyclerAdapter extends RecyclerView.Adapter<SampleRecyclerAdapter.ViewHolder> {

    private List<SampleModel> mList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sample, parent, false)
        );
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.getItemBinding().setItem(mList.get(position));
    }

    public void setList(List<SampleModel> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemSampleBinding mItemBinding;


        public ViewHolder(View itemView) {
            super(itemView);
            mItemBinding = DataBindingUtil.bind(itemView);
        }

        public ItemSampleBinding getItemBinding() {
            return mItemBinding;
        }
    }

}
