package com.urancompany.challenge;

import android.app.Application;

import com.urancompany.challenge.models.db.DatabaseManager;


/**
 * Created by ovi on 4/18/16.
 */
public class App extends Application {

    private static App sInstance;

    public static App getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;


        DatabaseManager.getsInstance().init(getApplicationContext());
    }
}
