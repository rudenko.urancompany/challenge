package com.urancompany.challenge.views;

import android.content.Context;

import com.urancompany.challenge.models.entities.SampleModel;

import java.util.List;

/**
 * Created by ovi on 4/18/16.
 */
public interface ISampleListView {


    Context getContext();

    void setList(List<SampleModel> list);

}
