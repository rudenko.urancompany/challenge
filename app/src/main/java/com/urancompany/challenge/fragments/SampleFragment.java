package com.urancompany.challenge.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.urancompany.challenge.R;
import com.urancompany.challenge.adapters.SampleRecyclerAdapter;
import com.urancompany.challenge.models.entities.SampleModel;
import com.urancompany.challenge.presenters.SamplesPresenter;
import com.urancompany.challenge.views.ISampleListView;

import java.util.List;

/**
 * Created by ovi on 4/18/16.
 */
public class SampleFragment extends BaseFragment implements ISampleListView {


    private SampleRecyclerAdapter mSampleRecyclerAdapter;

    private SamplesPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sample, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSampleRecyclerAdapter = new SampleRecyclerAdapter();

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mSampleRecyclerAdapter);

        mPresenter = new SamplesPresenter(getSpiceManager(), getLoaderManager(), this);
    }

    @Override
    public void setList(List<SampleModel> list) {
        mSampleRecyclerAdapter.setList(list);
    }
}
