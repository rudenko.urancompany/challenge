package com.urancompany.challenge.fragments;

import android.app.Fragment;

import com.octo.android.robospice.SpiceManager;
import com.urancompany.challenge.models.api.AppService;

/**
 * Created by ovi on 4/18/16.
 */
public class BaseFragment extends Fragment {

    private SpiceManager mSpiceManager = new SpiceManager(AppService.class);

    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }


    @Override
    public void onStart() {
        super.onStart();
        mSpiceManager.start(getActivity());
    }

    @Override
    public void onStop() {
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }

        super.onStop();
    }
}
