package com.urancompany.challenge.presenters;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;

import com.octo.android.robospice.SpiceManager;
import com.urancompany.challenge.models.api.listeners.SimpleRequestListener;
import com.urancompany.challenge.models.api.requests.GetSampleRequest;
import com.urancompany.challenge.models.db.loaders.SampleListLoader;
import com.urancompany.challenge.models.db.loaders.SimpleLoaderCallbacks;
import com.urancompany.challenge.models.entities.SampleModel;
import com.urancompany.challenge.views.ISampleListView;

import java.util.List;

/**
 * Created by ovi on 4/18/16.
 */
public class SamplesPresenter {

    private final int LOADER_ID = 100;

    private SpiceManager mSpiceManager;
    private LoaderManager mLoaderManager;

    private ISampleListView mSampleListView;

    public SamplesPresenter(SpiceManager spiceManager, LoaderManager loaderManager, ISampleListView sampleListView) {
        mSpiceManager = spiceManager;
        mLoaderManager = loaderManager;
        mSampleListView = sampleListView;


        loadFromDb();
        loadFromServer();
    }

    private void loadFromDb() {

        mLoaderManager.restartLoader(LOADER_ID, Bundle.EMPTY, new SimpleLoaderCallbacks<List<SampleModel>>() {
            @Override
            public Loader<List<SampleModel>> onCreateLoader(int id, Bundle args) {
                return new SampleListLoader(mSampleListView.getContext());
            }

            @Override
            public void onLoadFinished(Loader<List<SampleModel>> loader, List<SampleModel> data) {
                mSampleListView.setList(data);
            }
        });

    }

    private void loadFromServer() {
        mSpiceManager.execute(new GetSampleRequest(), new SimpleRequestListener<SampleModel[]>() {
            @Override
            public void onRequestSuccess(SampleModel[] response) {
                loadFromDb();
            }
        });
    }
}
